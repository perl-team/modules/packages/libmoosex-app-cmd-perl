libmoosex-app-cmd-perl (0.34-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 01:28:51 +0100

libmoosex-app-cmd-perl (0.34-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove franck cuny from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Import upstream version 0.34.
  * Update debian/upstream/metadata.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints and alternatives from (build)
    dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Fri, 19 Mar 2021 19:57:51 +0100

libmoosex-app-cmd-perl (0.32-2) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Damyan Ivanov ]
  * Swap the order of the alternative Test::Simple build-dependency
  * Claim conformance with Policy 3.9.7

 -- Damyan Ivanov <dmn@debian.org>  Mon, 22 Feb 2016 07:55:47 +0000

libmoosex-app-cmd-perl (0.32-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.
  * Add debian/upstream/metadata.

  * Import upstream version 0.32:
    Fixes "FTBFS: (Wstat: 256 Tests: 1 Failed: 1)"
    (Closes: #799763)
    Note that MouseX::App::Cmd is split off into a separate CPAN distribution.
  * Drop fix-test-regexps.patch, which was taken from upstream.
  * debian/copyright: update copyright statement, upstream contact,
    packaging years.
  * Update (build) dependencies.
  * Install new CONTRIBUTING file.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Tue, 22 Sep 2015 19:57:00 +0200

libmoosex-app-cmd-perl (0.27-2) unstable; urgency=medium

  * Strip trailing slash from metacpan URLs.
  * Add patch to update regexps in tests.
    Thanks to David Suárez for the bug report. (Closes: #750305)

 -- gregor herrmann <gregoa@debian.org>  Mon, 02 Jun 2014 23:35:52 +0200

libmoosex-app-cmd-perl (0.27-1) unstable; urgency=medium

  * New upstream release.
  * Update years of packaging copyright.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Fri, 03 Jan 2014 18:19:57 +0100

libmoosex-app-cmd-perl (0.11-1) unstable; urgency=low

  [ gregor herrmann ]
  * New upstream releases 0.08, 0.09.
  * Update years of upstream and packaging copyright.
  * Set debhelper compatibility level to 8.
  * Make (build) dependency on libgetopt-long-descriptive-perl versioned.
  * Add /me to Uploaders.
  * Add new (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Damyan Ivanov ]
  * Imported Upstream version 0.10
  * bump years of upstream copyright
  * bump version of libmoosex-configfromfile-perl (build-)dependency
  * drop libtest-requires-perl drop b-d

  [ gregor herrmann ]
  * New upstream release 0.11.
    Closes: #730898
  * Update (build) dependencies.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Nov 2013 21:53:02 +0100

libmoosex-app-cmd-perl (0.07-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Email change: Salvatore Bonaccorso -> carnil@debian.org
  * Imported Upstream version 0.07
  * Simplify versioned dependencies already satisfied in Stable.
    Make versioned (Build-)Depends(-Indep) which are already satisfied in
    stable now unversioned as Lenny/oldstable is going to the archive.
  * Update debian/copyright information.
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3.
    Update copyright years for debian/* packaging files.
    Refer to Debian systems in general instead of only Debian GNU/Linux
    systems.
    Explicitly point to GPL-1 license text in common-licenses.
  * Bump Standards-Version to 3.9.3
  * Convert to '3.0 (quilt)' source package format
  * Fix spelling in long description.
    Fix use of allows one to in long description reported by lintian checks.

 -- Salvatore Bonaccorso <carnil@debian.org>  Fri, 02 Mar 2012 08:40:20 +0100

libmoosex-app-cmd-perl (0.06-1) unstable; urgency=low

  * New upstream release
    - various fixes for upstream version of App::Cmd
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).
  * Add versioned dependency on libapp-cmd-perl (>= 0.3).
  * Bump Standards-Version to 3.8.3 (no changes)

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Fri, 18 Sep 2009 08:19:03 +0200

libmoosex-app-cmd-perl (0.05-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * New upstream release
  * debian/control:
    - Adjust versioned dependencies on libmoose-perl and bmoosex-getopt-perl.
    - Add myself to Uploaders
    - Bump Standards-Version to 3.8.2 (no changes)
  * debian/watch: Clean comments in watch-file.
  * debian/rules: Use tiny rules file.
  * debian/copyright: Adjust debian/* packaging copyright.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Mon, 06 Jul 2009 20:58:46 +0200

libmoosex-app-cmd-perl (0.04-1) unstable; urgency=low

  * Initial Release (Closes: #531398).

 -- franck cuny <franck@lumberjaph.net>  Mon, 1 Jun 2009 10:45:46 +0200
